const r = Date.parse('25 July 2018 09:00:00')
const d = Date.now()
const dif = r - d

const hd = Math.floor(dif/1000/60/60)
const l = dif - hd * 1000 * 60 * 60
const md = Math.floor(l/1000/60)
const s = l - md * 1000 * 60
const sd = Math.floor(s/1000)
console.log(`You're waking up in %s hours, %s minutes, %s seconds`, hd, md, sd)

// gifsicle at3.gif `seq -f "#%g" 0 1 66` -d200 "#67" --same-delay `seq -f "#%g" 68 1 69` > at33.gif
