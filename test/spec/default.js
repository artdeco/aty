import SnapshotContext from 'snapshot-context'
import Context from '../context'
import aty, { activateApp, type, typeInstant } from '../../src'

/** @type {Object.<string, (c: Context, s: SnapshotContext)>} */
const T = {
  context: [
    Context,
    SnapshotContext,
  ],
  async 'calls package without error'() {
    await aty()
  },
  async 'creates a script'({ SNAPSHOT_DIR }, { setDir, test }) {
    setDir(SNAPSHOT_DIR)
    const s = 'Hello World'
    const res = aty`
${activateApp`Code`}
${type`Hello World`}
${type`${s}`}
${type`Hello World${100}${200}`}
${type`${s}${10}${20}`}

${typeInstant`Hello World`}
${typeInstant`${s}`}
`
    await test('script.scpt', res)
  },
}

export default T
