on type(the_string, delay_from, delay_to)
  set theList to paragraphs of the_string
  set listCount to count of theList

  repeat with i from 1 to listCount
    tell application "System Events"
      repeat with c in item i of theList
        keystroke c
        delay (random number from delay_from to delay_to)
      end repeat
      if i is not listCount then key code 36
    end tell
  end repeat
end type
on typeInstant(the_string)
  tell application "System Events"
    keystroke the_string
    key code 36
  end tell
end type
activate application "Code"
type ("Hello World", 0.05, 0.1)
type ("Hello World", 0.05, 0.1)
type ("Hello World", 0.1, 0.2)
type ("Hello World", 0.01, 0.02)
typeInstant ("Hello World")
typeInstant ("Hello World")