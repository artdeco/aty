# aty

[![npm version](https://badge.fury.io/js/aty.svg)](https://npmjs.org/package/aty)

`aty` is a Node.js package that allows for automatic typing of strings into programs on Mac.

## Installation

`aty` can be either installed globally, or as a library.

<table>
<thead>
 <tr>
  <th>Command</th>
  <th>Use case</th>
  <th>Example</th>
 </tr>
</thead>

<tbody>
 <tr>
  <td align="center">
   <br/><em>npm i -g aty</em><br/><br/>
  </td>
  <td>Install as a global binary from <a href="#cli">CLI</a> and use to activate an application and type text into it.</td>
  <td>
   <code>aty "echo hello world\n" -a Terminal</code>
  </td>
 </tr>
 <tr>
  <td align="center">
   <br/><em>

```sh
yarn add -DE aty
```
</em>
  </td>
  <td>Install as a dependency and use <a href="#api">API</a> to run scripts.</td>
  <td>
   <code>node atyscript</code>
  </td>
 </tr>
</table>

## Demo

The demo below shows how one can use `aty` to automatically type a program which they just wrote.

<table>
<tbody>
<tr>
</tr>
<tr>
<td><a name="programming-like-a-hacker-with-aty">Programming like a hacker with aty</a></td>
</tr>
<tr>
<td><img src="doc/appshot-aty2.gif" alt="Harry you're a hacker - writing a program with aty."></td>
</tr>
</tbody></table>

## Table Of Contents

- [Installation](#installation)
- [Demo](#demo)
  * [Programming like a hacker with aty](#programming-like-a-hacker-with-aty)
- [Table Of Contents](#table-of-contents)
- [API](#api)
  * [`e(lines: string[])`](#elines-string-void)
  * [`w()`](#w-void)
  * [`aty(code: string): string`](#atycode-string-string)
  * [`activateApp(name: string): string`](#activateappname-string-string)
  * [`type(string: string, delayFrom?: number = 50, delayTo?: number = 100, noIndent?: boolean = false): string`](#typestring-stringdelayfrom-number--50delayto-number--100noindent-boolean--false-string)
  * [`typeInstant(string: string): string`](#typeinstantstring-string-string)
  * [`delay(time: number): string`](#delaytime-number-string)
  * [`keystroke(word: string, commands?: string[]): string`](#keystrokeword-stringcommands-string-string)
  * [`code(code: string, commands?: string[]): string`](#codecode-stringcommands-string-string)
- [API Examples](#api-examples)
  * [Register Domain For Package: <code>mnp-expensive</code>](#register-domain-for-package-mnp-expensive)
- [Key Code Reference Table](#key-code-reference-table)
- [CLI](#cli)
  * [Accepted Arguments](#accepted-arguments)
    * [<strong><code>-t, --text*</code></strong>](#-t---text)
    * [<strong><code>-a, --app*</code></strong>](#-a---app)
    * [<code>-i, --instant</code>](#-i---instant)
    * [<code>-d, --minDelay</code>](#-d---mindelay)
    * [<code>-m, --maxDelay</code>](#-m---maxdelay)
    * [<code>-h, --help</code>](#-h---help)
    * [<code>-v, --version</code>](#-v---version)
- [CLI Examples](#cli-examples)
  * [<code>aty "echo test\n" -i date -d 100 -a Terminal</code>](#aty-echo-testn--i-date--d-100--a-terminal)
- [Copyright](#copyright)

## API

The package is available by importing:

- its default function `aty` for generating an Apple Script code to be executed;
- the `exec` command to execute it;
- the `e` function which is a combination of `aty` and `exec`;
- the `w` function to pause execution of the script and wait for user input;
- various tagged templates to compose code for the script (e.g., such as `type`, `activateApplication`, `delay`, `code`, _etc_).

```js
import aty, {
  exec, e, w, activateApp, type, typeInstant, delay, keystroke, code,
} from 'aty'
```

```js
/* yarn example */
import aty, { exec, type, typeInstant, activateApp } from 'aty'

const s = `Hello World! \\
This is a new application that can type strings.`

;(async () => {
  const a = aty`
${activateApp`Code`}
${typeInstant`Welcome.`}
${type`${s}${10}${20}`}
  `
  await exec(a)
})()
```

### `e(`<br/>&nbsp;&nbsp;`lines: string[],`<br/>`): void`

Execute commands from the passed array. It is an alternative way to write scripts which does not require to import `aty` and `exec`.

```javascript
/* yarn example/e.js */
import { e, activateApp, type } from 'aty'

(async () => {
  await e([
    activateApp`Terminal`,
    type`echo hello world\n`,
  ])
})()
```

### `w(): void`

Wait for user to enter something via `stdin`. This command allows to pause executing a script, e.g., when it is necessary to wait for the result of typing, however how long to wait is unknown.

```javascript
/* yarn example/e.js */
import { e, w, activateApp, type } from 'aty'

(async () => {
  await e([
    activateApp`Terminal`,
    type`echo hello world\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`date\n`,
  ])
})()
```

### `aty(`<br/>&nbsp;&nbsp;`code: string,`<br/>`): string`

This tagged template is used to generate the total code to be executed. It will include necessary types, and put all other blocks together. It should be used to generate a string to pass to the `exec` method.

```js
/* yarn example/hacker.js */
import { readFileSync } from 'fs'
import { resolve } from 'path'
import aty, {
  type, typeInstant, activateApp, code, keystroke, delay,
} from 'aty'

const f = readFileSync(resolve(__dirname, __filename))

const a = aty`
${activateApp`Code`}
${typeInstant`/* recorded with appshot */`}
${type`${f}${15}${20}${true}`}
${keystroke`${'option'}${'shift'}f`}
${delay`2000`}
${keystroke`${'command'}a`}
${delay`1000`}
${code`51`}`

console.log(a)
```

```applescript
on type(the_string, delay_from, delay_to)
  set theList to paragraphs of the_string
  set listCount to count of theList

  repeat with i from 1 to listCount
    tell application "System Events"
      repeat with c in item i of theList
        keystroke c
        delay (random number from delay_from to delay_to)
      end repeat
      if i is not listCount then key code 36
    end tell
  end repeat
end type
on typeInstant(the_string)
  tell application "System Events"
    keystroke the_string
    key code 36
  end tell
end type
activate application "Code"
typeInstant ("/* recorded with appshot */")
type ("/* yarn example/hacker.js */
import { readFileSync } from 'fs'
import { resolve } from 'path'
import aty, {
type, typeInstant, activateApp, code, keystroke, delay,
} from 'aty'

const f = readFileSync(resolve(__dirname, __filename))

const a = aty`
${activateApp`Code`}
${typeInstant`/* recorded with appshot */`}
${type`${f}${15}${20}${true}`}
${keystroke`${'option'}${'shift'}f`}
${delay`2000`}
${keystroke`${'command'}a`}
${delay`1000`}
${code`51`}`

console.log(a)", 0.015, 0.02)
tell application "System Events" to keystroke "f" using {option down, shift down}
delay 2
tell application "System Events" to keystroke "a" using command down
delay 1
tell application "System Events" to key code 51
```

### `activateApp(`<br/>&nbsp;&nbsp;`name: string,`<br/>`): string`

Activate an app with the given name.

```js
/* yarn example/activate-app.js */
import { activateApp } from 'aty'

const app = 'Terminal'

const a = activateApp`${app}`
const b = activateApp`Code`

console.log(a)
console.log(b)
```

```applescript
activate application "Terminal"
activate application "Code"
```

### `type(`<br/>&nbsp;&nbsp;`string: string,`<br/>&nbsp;&nbsp;`delayFrom?: number = 50,`<br/>&nbsp;&nbsp;`delayTo?: number = 100,`<br/>&nbsp;&nbsp;`noIndent?: boolean = false,`<br/>`): string`

The `type` template is used to generate a command to type a text into an application. It can be used after `activateApp` command to send text to a particular application. `aty` will automatically include the `type` function when `type` was used in the script. `noIndent` can be used to replace white spaces in the beginning of each line.

```js
/* yarn example/type.js */
import aty, { type } from 'aty'

const t = 'admin\\n'

const a = type`${t}`
const b = type`Though sympathy alone can't alter facts,
  it can help to make them more bearable.

${50}${100}${true}`

console.log(aty`
${a}
${b}
`)
```

```applescript
on type(the_string, delay_from, delay_to)
  set theList to paragraphs of the_string
  set listCount to count of theList

  repeat with i from 1 to listCount
    tell application "System Events"
      repeat with c in item i of theList
        keystroke c
        delay (random number from delay_from to delay_to)
      end repeat
      if i is not listCount then key code 36
    end tell
  end repeat
end type
type ("admin\n", 0.05, 0.1)
type ("Though sympathy alone can't alter facts,
it can help to make them more bearable.
", 0.05, 0.1)
```


### `typeInstant(`<br/>&nbsp;&nbsp;`string: string,`<br/>`): string`

Same as `type`, but will insert text immediately without a delay. When `aty` sees a string which begins with `typeInstant`, it will automatically include the `typeInstant` function.

```js
/* yarn example/type-instant.js */
import aty, { typeInstant } from 'aty'

const t = 'Type Instant Text'

const a = typeInstant`${t}`
const b = typeInstant`Do you believe in destiny?
That even the powers of time can be altered for a single purpose?`

console.log(aty`
${a}
${b}
`)
```

```applescript
on typeInstant(the_string)
  tell application "System Events"
    keystroke the_string
    key code 36
  end tell
end type
typeInstant ("Type Instant Text")
typeInstant ("Do you believe in destiny?
That even the powers of time can be altered for a single purpose?")
```
### `delay(`<br/>&nbsp;&nbsp;`time: number,`<br/>`): string`

Delay execution by `n` milliseconds.

```js
/* yarn example/delay.js */
import { delay } from 'aty'

const d = 100

const a = delay`${d}`
const b = delay`500`

console.log(a)
console.log(b)
```

```applescript
delay 0.1
delay 0.5
```

### `keystroke(`<br/>&nbsp;&nbsp;`word: string,`<br/>&nbsp;&nbsp;`commands?: string[],`<br/>`): string`

Send a key stroke. If commands are provided, they will be included.

```js
/* yarn example/keystroke.js */
import { keystroke } from 'aty'

const a = keystroke`a${'command'}`
const b = keystroke`s${'alt'}${'command'}`

console.log(a)
console.log(b)
```

```applescript
tell application "System Events" to keystroke "a" using command down
tell application "System Events" to keystroke "s" using {alt down, command down}
```

### `code(`<br/>&nbsp;&nbsp;`code: string,`<br/>&nbsp;&nbsp;`commands?: string[],`<br/>`): string`

Send a code. If commands are provided, they will be included with `using` keyword. Some codes can be found in the [Key Code Reference Table](#key-code-reference-table) below.

```js
/* yarn example/code.js */
import { code } from 'aty'

const a = code`36${'command'}`
const b = code`36${'command'}${'shift'}`

console.log(a)
console.log(b)
```

```applescript
tell application "System Events" to key code 36 using command down
tell application "System Events" to key code 36 using {command down, shift down}
```

## API Examples

<strong><a name="register-domain-for-package-mnp-expensive">Register Domain For Package: <code>mnp-expensive</code></a></strong>: This example shows how to activate the Terminal app, and execute 4 commands, waiting for user-input to begin each, i.e., when any key rather than <code>n</code> is entered, <code>aty</code> will continue execution.

<table>
<thead>
<tr><th>JS Code</th><th>Generated Code</th></tr>
</thead>
<tbody>
<tr></tr>
<tr><td>

```javascript
/* yarn scripts/alamode.js */
import { e, w, activateApp,
  type } from '../src'

(async () => {
  await e([
    activateApp`Terminal`,
    type`mnp alamode -c\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`expensive alamode\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`expensive alamode.app -r\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`y\n`,
  ])
})()
```
</td><td>

```applescript
# mnp-expensive.scpt

set type to "aty:Type.scpt" as alias



activate application "Terminal"
type ("mnp alamode -c\n", 0.05, 0.1)


# waits for enter
activate application "Terminal"
type ("expensive alamode\n", 0.05, 0.1)


# waits for enter
activate application "Terminal"
type ("expensive alamode.app -r\n", 0.05, 0.1)


# waits for enter
activate application "Terminal"
type ("y\n", 0.05, 0.1)

#
```
</td></tr>

</tr>

<tr><td align="center" colspan="2"><strong>Example</strong></td></tr>
<tr></tr>
 <tr>
  <td colspan="2">

<details>
  <summary>Click to View</summary>

![Quick package and domain names check, registering a domain.](doc/appshot-mnp-expensive.gif)
</details>
  </td>
</tr>

</tbody>
    </table>

## Key Code Reference Table

<table>
<thead>
<tr></tr>
<tr><td colspan="7">

```
This table can be used for a quick look up of a key code.
```
</td></tr>
</thead>
<tbody>
<tr>
  <th>Key</th> <th>Name</th> <th>Code</th> <th rowspan="8">&nbsp;</th> <th>Key</th> <th>Name</th> <th>Code</th>
</tr>
<tr>
  <td>
   <kbd>⎋</kbd>
  </td>
  <td>
   escape
  </td>
  <td>
   53
  </td>
  <td>
   <kbd>&nbsp;! <br/>&nbsp;1 </kbd>
  </td>
  <td>
   key 1
  </td>
  <td>
   18
  </td>

</tr>
<tr>
  <td>
   <kbd>⇥</kbd>
  </td>
  <td>
   tab
  </td>
  <td>
   48
  </td>
  <td>
   <kbd>&nbsp; &nbsp; &nbsp;</kbd>
  </td>
  <td>
   spacebar
  </td>
  <td>
   49
  </td>

</tr>
<tr>
  <td>
   <kbd>↩</kbd>
  </td>
  <td>
   return
  </td>
  <td>
   36
  </td>
  <td>
   <kbd>⇞</kbd>
  </td>
  <td>
   page up
  </td>
  <td>
   116
  </td>

</tr>
<tr>
  <td>
   <kbd>⌤</kbd>
  </td>
  <td>
   enter
  </td>
  <td>
   76
  </td>
  <td>
   <kbd>⇟</kbd>
  </td>
  <td>
   page down
  </td>
  <td>
   121
  </td>

</tr>
<tr>
  <td>
   <kbd>⌃</kbd>
  </td>
  <td>
   left ctrl
  </td>
  <td>
   59
  </td>
  <td>
   <kbd>⇧</kbd>
  </td>
  <td>
   left shift
  </td>
  <td>
   57
  </td>

</tr>
<tr>
  <td>
   <kbd>⌥</kbd>
  </td>
  <td>
   left option
  </td>
  <td>
   58
  </td>
  <td>
   <kbd>→</kbd>
  </td>
  <td>
   right arrow
  </td>
  <td>
   124
  </td>
</tr>
<tr>
  <td>
   <kbd>⌘</kbd>
  </td>
  <td>
   left command
  </td>
  <td>
   55
  </td>
  <td>
   <kbd>⇪</kbd>
  </td>
  <td>
   capslock
  </td>
  <td>
   57
  </td>
</tr>
<tr>
  <td colspan="7"><h5>Find more at:</h5>

- [Complete list of AppleScript key codes](https://eastmanreference.com/complete-list-of-applescript-key-codes)
- [Javascript Char Codes (Key Codes)](https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes)
- [HTML Entity - GLYPH - NAME chart](https://apple.stackexchange.com/questions/55727/where-can-i-find-the-unicode-symbols-for-mac-functional-keys-command-shift-e)
  </td>
</tr>
</tbody>
</table>


## CLI

The package can be used from the `CLI` if installed globally. It can generate an apple script to activate an app specified with `-a` argument, and type text into it.

```sh
aty -h
```

<table><tr></tr>

<tr><td>

```fs
Activate an app with AppleScript and type some text there.
If -i (--instant) is given, it will be instantly typed  before
the actual text. 

  aty "test text" -a appToActivate -i "instant text" [-d 10] [-m 50] [-hv]

	text          	The text to type.
	-a, --app     	The app which to open.
	-i, --instant 	Text to type instantly, before text
	-d, --minDelay	The minimum delay with which to type.
	-m, --maxDelay	The maximum delay with which to type.
	-h, --help    	Display help and quit.
	-v, --version 	Show version.

  Example:

    aty "echo test\n" -i "date" -a iTerm
```
</td></tr>

</table>

### Accepted Arguments

Arguments can be passed by specifying their value `-arg value`, e.g., `-a app`.

<table>
 <thead>
  <tr>
   <th>Property</th>
   <th>Type</th>
   <th>Description</th>
   <th>Example</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td><a name="-t---text"><strong><code>-t, --text*</code></strong></a></td>
   <td><em>string</em></td>
   <td>The text to type. New line (<code>\n</code>) chars will be used to press Enter key.</td>
   <td><code>echo hello world\n</code></td>
  </tr>
  <tr>
   <td><a name="-a---app"><strong><code>-a, --app*</code></strong></a></td>
   <td><em>string</em></td>
   <td>Application to activate before typing.</td>
   <td><code>Terminal</code></td>
  </tr>
  <tr>
   <td><a name="-i---instant"><code>-i, --instant</code></a></td>
   <td><em>string</em></td>
   <td>Text to enter without a delay after activating the app but before typing the text.</td>
   <td><code>date</code></td>
  </tr>
  <tr>
   <td><a name="-d---mindelay"><code>-d, --minDelay</code></a></td>
   <td><em>number</em></td>
   <td>The minimum delay with which to type. Default <code>50ms</code>.</td>
   <td><code>200</code> for 200 ms.</td>
  </tr>
  <tr>
   <td><a name="-m---maxdelay"><code>-m, --maxDelay</code></a></td>
   <td><em>number</em></td>
   <td>The maximum delay with which to type. Default <code>100ms</code>.</td>
   <td><code>500</code> for 500 ms.</td>
  </tr>
  <tr>
   <td><a name="-h---help"><code>-h, --help</code></a></td>
   <td><em>boolean</em></td>
   <td>Show help information.</td>
   <td><code>aty -h</code></td>
  </tr>
  <tr>
   <td><a name="-v---version"><code>-v, --version</code></a></td>
   <td><em>boolean</em></td>
   <td>Show version.</td>
   <td><code>aty -v</code></td>
  </tr>
 </tbody>
</table>


## CLI Examples

<details>
  <summary>Click to View: <a name="aty-echo-testn--i-date--d-100--a-terminal"><code>aty "echo test\n" -i date -d 100 -a Terminal</code></a></summary>
  <table>
  <tr><td>
    <img alt="Alt: Running aty executable." src="doc/aty.gif" />
  </td></tr>
  </table>
</details>


## Copyright

(c) [Art Deco][1] 2018

[1]: https://artdeco.bz
