"use strict";

var _assert = require("assert");

var _argufy = _interopRequireDefault(require("argufy"));

var _usually = _interopRequireDefault(require("usually"));

var _ = _interopRequireWildcard(require(".."));

var _package = require("../../package.json");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @type {Arguments} */
const {
  text,
  app,
  minDelay = 10,
  maxDelay = 50,
  instant,
  help,
  version
} = (0, _argufy.default)({
  text: {
    command: true,
    required: true
  },
  app: {
    short: 'a',
    required: true
  },
  instant: {
    short: 'i'
  },
  minDelay: {
    short: 'd',
    number: true
  },
  maxDelay: {
    short: 'm',
    number: true
  },
  help: {
    short: 'h',
    boolean: true
  },
  version: {
    short: 'v',
    boolean: true
  }
});

if (version) {
  console.log(_package.version);
  process.exit();
}

if (help) {
  const h = (0, _usually.default)({
    usage: {
      text: 'The text to type.',
      '-a, --app': 'The app which to open.',
      '-i, --instant': 'Text to type instantly, before text',
      '-d, --minDelay': 'The minimum delay with which to type.',
      '-m, --maxDelay': 'The maximum delay with which to type.',
      '-h, --help': 'Display help and quit.',
      '-v, --version': 'Show version.'
    },
    description: `Activate an app with AppleScript and type some text there.
If -i (--instant) is given, it will be instantly typed  before
the actual text. `,
    line: 'aty "test text" -a appToActivate -i "instant text" [-d 10] [-m 50] [-hv]',
    example: 'aty "echo test\\n" -i "date" -a iTerm'
  });
  console.log(h);
  process.exit(1);
}

const run = async () => {
  try {
    (0, _assert.ok)(text, 'Please give text');
    (0, _assert.ok)(app, 'Please give app');
    const a = _.default`
${_.activateApp`${app}`}
${instant ? `${_.typeInstant`${instant}`}
     ${_.delay`1000`}` : ''}
${_.type`${text}${minDelay}${maxDelay}`}
`; // ${code`36`}

    await (0, _.exec)(a);
  } catch (err) {
    console.log(err);
  }
};

(async () => {
  await run();
})();
/**
 * @typedef {Object} Arguments
 * @prop {string} text The text to type.
 * @prop {string} app The app which to open.
 * @prop {string} [instant] Text to type instantly, before text.
 * @prop {number} [minDelay=10] The minimum delay with which to type.
 * @prop {number} [maxDelay=50] The maximum delay with which to type.
 * @prop {boolean} [help=false] Display help and quit.
 */
//# sourceMappingURL=index.js.map