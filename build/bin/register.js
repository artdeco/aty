"use strict";

const {
  resolve
} = require('path');

const cwd = resolve(__dirname, '../..');

require('@babel/register')({
  cwd
});

require('.');
//# sourceMappingURL=register.js.map