"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = aty;
Object.defineProperty(exports, "exec", {
  enumerable: true,
  get: function () {
    return _lib.exec;
  }
});
Object.defineProperty(exports, "type", {
  enumerable: true,
  get: function () {
    return _lib.type;
  }
});
Object.defineProperty(exports, "typeInstant", {
  enumerable: true,
  get: function () {
    return _lib.typeInstant;
  }
});
Object.defineProperty(exports, "activateApp", {
  enumerable: true,
  get: function () {
    return _lib.activateApp;
  }
});
Object.defineProperty(exports, "keystroke", {
  enumerable: true,
  get: function () {
    return _lib.keystroke;
  }
});
Object.defineProperty(exports, "code", {
  enumerable: true,
  get: function () {
    return _lib.code;
  }
});
Object.defineProperty(exports, "delay", {
  enumerable: true,
  get: function () {
    return _lib.delay;
  }
});
Object.defineProperty(exports, "typeVar", {
  enumerable: true,
  get: function () {
    return _lib.typeVar;
  }
});
exports.e = exports.w = void 0;

var _util = require("util");

var _lib = require("./lib");

var _reloquent = require("reloquent");

const LOG = (0, _util.debuglog)('aty');
/**
 * Automatic typing of strings into programs on Mac for Node.js.
 * A tagged template function. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
 * @param {string[]} raw Any raw strings will not be used.
 * @param {string[]} strings Commands to the automatic type.
 */

function aty(raw, ...strings) {
  LOG(strings);
  const hasType = strings.some(a => a.startsWith('type '));
  const hasTypeInstant = strings.some(a => a.startsWith('typeInstant '));
  const types = [hasType && _lib.typeDef, hasTypeInstant && _lib.typeInstantDef].filter(a => a);
  const def = _lib.Tr`${types.join('\n')}`;
  const s = _lib.Tr`
${def}
${strings.join('\n')}
`;
  return s;
}
/**
 * Wait for `stdin` to receive input to continue the execution. If `n` is given, the program returns by throwing a `no continue` exception.
 */


const w = async () => {
  const a = await (0, _reloquent.askSingle)({
    text: 'continue y/n',
    defaultValue: 'y'
  });
  if (a == 'n') throw new Error('no continue');
};
/**
 * Create apple script lines with `aty`, and execute them.
 * @param {string[]} lines The lines to execute.
 */


exports.w = w;

const e = async lines => {
  const a = aty([], ...lines);
  await (0, _lib.exec)(a);
};
/**
 * @typedef {Object} Config
 * @property {string} type The type.
 */


exports.e = e;
//# sourceMappingURL=index.js.map