"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.T = T;
exports.Tr = Tr;
exports.exec = exec;
exports.activateApp = exports.delay = exports.typeInstant = exports.type = exports.getKeys = exports.code = exports.keystroke = exports.typeDef = exports.typeInstantDef = void 0;

var _spawncommand = _interopRequireDefault(require("spawncommand"));

var _util = require("util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LOG = (0, _util.debuglog)('aty');

function T({
  raw: [s]
}) {
  return s.trim();
}

function Tr(raw, ...strings) {
  const s = strings.map(a => {
    return a.trim();
  }).join('\n');
  return s;
}

const typeInstantDef = T`
on typeInstant(the_string)
  tell application "System Events"
    keystroke the_string
    key code 36
  end tell
end type`;
exports.typeInstantDef = typeInstantDef;
const typeDef = T`
on type(the_string, delay_from, delay_to)
  set theList to paragraphs of the_string
  set listCount to count of theList

  repeat with i from 1 to listCount
    tell application "System Events"
      repeat with c in item i of theList
        keystroke c
        delay (random number from delay_from to delay_to)
      end repeat
      if i is not listCount then key code 36
    end tell
  end repeat
end type`;
exports.typeDef = typeDef;

const keystroke = ({
  raw
}, ...commands) => {
  const s = raw.find(a => a);
  const using = getUsing(commands);
  return `tell application "System Events" to keystroke "${s}"${using}`;
};

exports.keystroke = keystroke;

const code = ({
  raw
}, ...commands) => {
  const s = raw.find(a => a);
  const using = getUsing(commands);
  return `tell application "System Events" to key code ${s}${using}`;
};

exports.code = code;

const getUsing = commands => {
  let s = '';
  if (commands.length == 1) s = ` using ${commands[0]} down`;else if (commands.length) s = ` using {${getKeys(commands)}}`;
  return s;
};

const getKeys = commands => {
  const keys = commands.map(c => `${c} down`).join(', ');
  return keys;
};

exports.getKeys = getKeys;

const type = (...args) => {
  const [{
    raw: [s]
  }, string, delayFrom, delayTo, noIndent] = args;
  let st = s ? `${s}` : `${string}`;
  let df = delayFrom || 50;
  let dt = delayTo || 100;
  let ni = noIndent || false;

  if (s) {
    df = string || 50;
    dt = delayFrom || 100;
    ni = delayTo || false;
  }

  const sst = ni ? st.replace(/^ +/mg, '') : st;
  return `type ("${sst.replace(/\n$/g, '')}", ${df / 1000}, ${dt / 1000})`;
};

exports.type = type;

const typeInstant = (...args) => {
  const st = getArg(args);
  return `typeInstant ("${st}")`;
};

exports.typeInstant = typeInstant;

const delay = (...args) => {
  const d = getArg(args);
  return `delay ${d / 1000}`;
};

exports.delay = delay;

const getArg = args => {
  const [{
    raw: [arg1]
  }, arg2] = args;
  return arg2 || arg1;
};

const activateApp = (...args) => {
  LOG(args);
  const app = getArg(args);
  if (!app) return '';
  return `activate application "${app}"`;
};

exports.activateApp = activateApp;

async function exec(s) {
  LOG(s);
  const {
    promise: p
  } = (0, _spawncommand.default)('osascript', ['-e', s]);
  const {
    stdout,
    stderr
  } = await p;
  LOG(stdout);
  LOG(stderr);
}
//# sourceMappingURL=index.js.map