
## Key Code Reference Table

<table>
<thead>
<tr></tr>
<tr><td colspan="7">

```
This table can be used for a quick look up of a key code.
```
<!-- <h3>[Key Code Reference Table](##)</h3> -->
</td></tr>
</thead>
<tbody>
<tr>
  <th>Key</th> <th>Name</th> <th>Code</th> <th rowspan="8">&nbsp;</th> <th>Key</th> <th>Name</th> <th>Code</th>
</tr>
<tr>
  <td>
   <kbd>⎋</kbd>
  </td>
  <td>
   escape
  </td>
  <td>
   53
  </td>
  <td>
   <kbd>&nbsp;! <br/>&nbsp;1 </kbd>
  </td>
  <td>
   key 1
  </td>
  <td>
   18
  </td>

</tr>
<tr>
  <td>
   <kbd>⇥</kbd>
  </td>
  <td>
   tab
  </td>
  <td>
   48
  </td>
  <td>
   <kbd>&nbsp; &nbsp; &nbsp;</kbd>
  </td>
  <td>
   spacebar
  </td>
  <td>
   49
  </td>

</tr>
<tr>
  <td>
   <kbd>↩</kbd>
  </td>
  <td>
   return
  </td>
  <td>
   36
  </td>
  <td>
   <kbd>⇞</kbd>
  </td>
  <td>
   page up
  </td>
  <td>
   116
  </td>

</tr>
<tr>
  <td>
   <kbd>⌤</kbd>
  </td>
  <td>
   enter
  </td>
  <td>
   76
  </td>
  <td>
   <kbd>⇟</kbd>
  </td>
  <td>
   page down
  </td>
  <td>
   121
  </td>

</tr>
<tr>
  <td>
   <kbd>⌃</kbd>
  </td>
  <td>
   left ctrl
  </td>
  <td>
   59
  </td>
  <td>
   <kbd>⇧</kbd>
  </td>
  <td>
   left shift
  </td>
  <td>
   57
  </td>

</tr>
<tr>
  <td>
   <kbd>⌥</kbd>
  </td>
  <td>
   left option
  </td>
  <td>
   58
  </td>
  <td>
   <kbd>→</kbd>
  </td>
  <td>
   right arrow
  </td>
  <td>
   124
  </td>
</tr>
<tr>
  <td>
   <kbd>⌘</kbd>
  </td>
  <td>
   left command
  </td>
  <td>
   55
  </td>
  <td>
   <kbd>⇪</kbd>
  </td>
  <td>
   capslock
  </td>
  <td>
   57
  </td>
</tr>
<tr>
  <td colspan="7"><h5>Find more at:</h5>

- [Complete list of AppleScript key codes](https://eastmanreference.com/complete-list-of-applescript-key-codes)
- [Javascript Char Codes (Key Codes)](https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes)
- [HTML Entity - GLYPH - NAME chart](https://apple.stackexchange.com/questions/55727/where-can-i-find-the-unicode-symbols-for-mac-functional-keys-command-shift-e)
  </td>
</tr>
</tbody>
</table>

<!-- `/System/` `Library/` `Frameworks/` `Carbon.framework/` `Versions/` `A/` `Frameworks/` `HIToolbox.framework/` `Versions/` `A/` `Headers/` `Events.h` -->
