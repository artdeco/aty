
## API Examples

<!-- %GIF doc/appshot-mnp-expensive.gif
Alt: Quick package and domain names check, registering a domain.
Running <code>mnp -c packageName</code> to check package availability on <em>npm</em>, checking <code>packageName</code> domains and registering a <code>packageName.app</code> domain with the <code><code>expensive</code></code> CLI tool. Click to show - [Register a domain: <code>expensive domain.app -r</code>](t)
% -->
<strong>[Register Domain For Package: <code>mnp-expensive</code>](t)</strong>: This example shows how to activate the Terminal app, and execute 4 commands, waiting for user-input to begin each, i.e., when any key rather than <code>n</code> is entered, <code>aty</code> will continue execution.

<table>
<thead>
<tr><th>JS Code</th><th>Generated Code</th></tr>
</thead>
<tbody>
<tr></tr>
<tr><td>

%EXAMPLE: scripts/alamode.js, ../src => aty, javascript%
</td><td>

%EXAMPLE: scripts/alamode-comments.scpt, ../src => aty, applescript%
</td></tr>

<!-- <tr><td>
Running <em>mnp</em> to check name availability on npm, checking domains on <em>namecheap</em> and registering a domain with the <em>expensive</em> CLI tool.
</td></tr> -->
</tr>

<tr><td align="center" colspan="2"><strong>Example</strong></td></tr>
<tr></tr>
 <tr>
  <td colspan="2">

<details>
  <summary>Click to View</summary>

![Quick package and domain names check, registering a domain.](doc/appshot-mnp-expensive.gif)
</details>
  </td>
</tr>

</tbody>
<!-- <tr><td> -->

<!--
%GIF doc/appshot-mnp-expensive.gif
Alt: Quick package and domain names check, registering a domain.
Click to show - [Register a domain: <code>expensive domain.app -r</code>](t)
% -->

<!-- <details> -->
  <!-- <summary>Click to show - [Register a domain: <code>expensive domain.app -r</code>](t)</summary> -->
  <!-- <img alt="Alt: Quick package and domain names check, registering a domain." src="doc/appshot-mnp-expensive.gif" /> -->
<!-- </details> -->

<!-- </td></tr> -->

</table>
