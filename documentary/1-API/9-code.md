
```### code => string
[
  ["code", "string"],
  ["commands?", "string[]"]
]
```

Send a code. If commands are provided, they will be included with `using` keyword. Some codes can be found in the [Key Code Reference Table](#key-code-reference-table) below.

%EXAMPLE: example/code.js, ../src => aty, js%

%FORK-applescript example example/code.js%
