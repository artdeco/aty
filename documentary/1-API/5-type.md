
```### type => string
[
  ["string", "string"],
  ["delayFrom?", "number = 50"],
  ["delayTo?", "number = 100"],
  ["noIndent?", "boolean = false"]
]
```

The `type` template is used to generate a command to type a text into an application. It can be used after `activateApp` command to send text to a particular application. `aty` will automatically include the `type` function when `type` was used in the script. `noIndent` can be used to replace white spaces in the beginning of each line.

%EXAMPLE: example/type.js, ../src => aty, js%

%FORK-applescript example example/type.js%

<!-- ```js
const s = `Hello
World
`
${type`${s}${10}${20}`} // pass string from a variable
${type`${s}`} // pass string from a variable with default delay

${type`Hello
World
${10}${20}`} // pass raw string with delay
${type`Hello
World
`} // pass raw string with default delay
``` -->
