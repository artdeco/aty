
```### aty => string
[
  ["code", "string"]
]
```

This tagged template is used to generate the total code to be executed. It will include necessary types, and put all other blocks together. It should be used to generate a string to pass to the `exec` method.

%EXAMPLE: example/aty.js, ../src => aty%

%FORK-applescript example example/aty%
