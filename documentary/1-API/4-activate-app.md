
```### activateApp => string
[
  ["name", "string"]
]
```

Activate an app with the given name.

%EXAMPLE: example/activate-app.js, ../src => aty, js%

%FORK-applescript example example/activate-app.js%
