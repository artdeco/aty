
## API

The package is available by importing:

- its default function `aty` for generating an Apple Script code to be executed;
- the `exec` command to execute it;
- the `e` function which is a combination of `aty` and `exec`;
- the `w` function to pause execution of the script and wait for user input;
- various tagged templates to compose code for the script (e.g., such as `type`, `activateApplication`, `delay`, `code`, _etc_).

```js
import aty, {
  exec, e, w, activateApp, type, typeInstant, delay, keystroke, code,
} from 'aty'
```

%EXAMPLE: example/example.js, ../src => aty%
