
```### delay => string
[
  ["time", "number"]
]
```

Delay execution by `n` milliseconds.

%EXAMPLE: example/delay.js, ../src => aty, js%

%FORK-applescript example example/delay.js%
