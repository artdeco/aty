
```### keystroke => string
[
  ["word", "string"],
  ["commands?", "string[]"]
]
```

Send a key stroke. If commands are provided, they will be included.

%EXAMPLE: example/keystroke.js, ../src => aty, js%

%FORK-applescript example example/keystroke.js%
