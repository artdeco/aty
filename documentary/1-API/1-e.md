
```### e
[
  ["lines", "string[]"]
]
```

Execute commands from the passed array. It is an alternative way to write scripts which does not require to import `aty` and `exec`.

%EXAMPLE: example/e.js, ../src => aty, javascript%
