
```### typeInstant => string
[
  ["string", "string"]
]
```

Same as `type`, but will insert text immediately without a delay. When `aty` sees a string which begins with `typeInstant`, it will automatically include the `typeInstant` function.

<!-- ```applescript
on typeInstant(the_string)
  tell application "System Events"
    keystroke the_string
    key code 36
  end tell
end type
``` -->

%EXAMPLE: example/type-instant.js, ../src => aty, js%

%FORK-applescript example example/type-instant.js%