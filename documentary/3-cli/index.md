
## CLI

The package can be used from the `CLI` if installed globally. It can generate an apple script to activate an app specified with `-a` argument, and type text into it.

```sh
aty -h
```

<table><tr></tr>

<tr><td>

%FORK-fs src/bin/register -h%
</td></tr>

</table>

### Accepted Arguments

Arguments can be passed by specifying their value `-arg value`, e.g., `-a app`.

%TYPE true
<p name="-t, --text" type="string" required>
  <d>The text to type. New line (<code>\n</code>) chars will be used to press Enter key.</d>
  <e><code>echo hello world\n</code></e>
</p>
<p name="-a, --app" type="string" required>
  <d>Application to activate before typing.</d>
  <e><code>Terminal</code></e>
</p>
<p name="-i, --instant" type="string">
  <d>Text to enter without a delay after activating the app but before typing the text.</d>
  <e><code>date</code></e>
</p>
<p name="-d, --minDelay" type="number">
  <d>The minimum delay with which to type. Default <code>50ms</code>.</d>
  <e><code>200</code> for 200 ms.</e>
</p>
<p name="-m, --maxDelay" type="number">
  <d>The maximum delay with which to type. Default <code>100ms</code>.</d>
  <e><code>500</code> for 500 ms.</e>
</p>
<p name="-h, --help" type="boolean">
  <d>Show help information.</d>
  <e><code>aty -h</code></e>
</p>
<p name="-v, --version" type="boolean">
  <d>Show version.</d>
  <e><code>aty -v</code></e>
</p>
%

## CLI Examples

%GIF doc/aty.gif
Alt: Running aty executable.
Click to View: [<code>aty "echo test\n" -i date -d 100 -a Terminal</code>](t)
%

<!-- ### todo

- [] read from a file an pass to the application
- [] document more
- [] write tests -->
