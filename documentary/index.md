# aty

%NPM: aty%

`aty` is a Node.js package that allows for automatic typing of strings into programs on Mac.

## Installation

`aty` can be either installed globally, or as a library.

<table>
<thead>
 <tr>
  <th>Command</th>
  <th>Use case</th>
  <th>Example</th>
 </tr>
</thead>

<tbody>
 <tr>
  <td align="center">
   <br/><em>npm i -g aty</em><br/><br/>
  </td>
  <td>Install as a global binary from <a href="#cli">CLI</a> and use to activate an application and type text into it.</td>
  <td>
   <code>aty "echo hello world\n" -a Terminal</code>
  </td>
 </tr>
 <tr>
  <td align="center">
   <br/><em>

```sh
yarn add -DE aty
```
</em>
  </td>
  <td>Install as a dependency and use <a href="#api">API</a> to run scripts.</td>
  <td>
   <code>node atyscript</code>
  </td>
 </tr>
</table>

## Demo

The demo below shows how one can use `aty` to automatically type a program which they just wrote.

<table>
<tbody>
<tr>
</tr>
<tr>
<td>[Programming like a hacker with aty](t)</td>
</tr>
<tr>
<td><img src="doc/appshot-aty2.gif" alt="Harry you're a hacker - writing a program with aty."></td>
</tr>
</tbody></table>

## Table Of Contents

%TOC%
