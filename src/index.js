import { debuglog } from 'util'
import { Tr, typeDef, typeInstantDef, exec } from './lib'
import { askSingle } from 'reloquent'

const LOG = debuglog('aty')

/**
 * Automatic typing of strings into programs on Mac for Node.js.
 * A tagged template function. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
 * @param {string[]} raw Any raw strings will not be used.
 * @param {string[]} strings Commands to the automatic type.
 */
export default function aty(raw, ...strings) {
  LOG(strings)
  const hasType = strings.some(a => a.startsWith('type '))
  const hasTypeInstant = strings.some(a => a.startsWith('typeInstant '))
  const types = [hasType && typeDef, hasTypeInstant && typeInstantDef].filter(a => a)
  const def = Tr`${types.join('\n')}`

  const s = Tr`
${def}
${strings.join('\n')}
`
  return s
}

/**
 * Wait for `stdin` to receive input to continue the execution. If `n` is given, the program returns by throwing a `no continue` exception.
 */
export const w = async () => {
  const a = await askSingle({
    text: 'continue y/n',
    defaultValue: 'y',
  })
  if (a == 'n') throw new Error('no continue')
}

/**
 * Create apple script lines with `aty`, and execute them.
 * @param {string[]} lines The lines to execute.
 */
export const e = async (lines) => {
  const a = aty([], ...lines)
  await exec(a)
}

export { exec, type, typeInstant, activateApp, keystroke, code, delay, typeVar } from './lib'

/**
 * @typedef {Object} Config
 * @property {string} type The type.
 */
