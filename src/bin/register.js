const { resolve } = require('path')

const cwd = resolve(__dirname, '../..')

require('@babel/register')({ cwd })
require('.')
