import { ok } from 'assert'
import argufy from 'argufy'
import usually from 'usually'
import aty, { exec, type, typeInstant, activateApp, delay } from '..'
import { version as v } from '../../package.json'

/** @type {Arguments} */
const { text, app, minDelay = 10, maxDelay = 50, instant, help, version } = argufy({
  text: { command: true, required: true },
  app: { short: 'a', required: true },
  instant: { short: 'i' },
  minDelay: { short: 'd', number: true },
  maxDelay: { short: 'm', number: true },
  help: { short: 'h', boolean: true },
  version: { short: 'v', boolean: true },
})

if (version) {
  console.log(v)
  process.exit()
}

if (help) {
  const h = usually({
    usage: {
      text: 'The text to type.',
      '-a, --app': 'The app which to open.',
      '-i, --instant': 'Text to type instantly, before text',
      '-d, --minDelay': 'The minimum delay with which to type.',
      '-m, --maxDelay': 'The maximum delay with which to type.',
      '-h, --help': 'Display help and quit.',
      '-v, --version': 'Show version.',
    },
    description:
`Activate an app with AppleScript and type some text there.
If -i (--instant) is given, it will be instantly typed  before
the actual text. `,
    line: 'aty "test text" -a appToActivate -i "instant text" [-d 10] [-m 50] [-hv]',
    example: 'aty "echo test\\n" -i "date" -a iTerm',
  })
  console.log(h)
  process.exit(1)
}

const run = async () => {
  try {
    ok(text, 'Please give text')
    ok(app, 'Please give app')

    const a = aty`
${activateApp`${app}`}
${ instant ?
    `${typeInstant`${instant}`}
     ${delay`1000`}` : ''
}
${type`${text}${minDelay}${maxDelay}`}
`
    // ${code`36`}
    await exec(a)
  } catch (err) {
    console.log(err)
  }
}

(async () => {
  await run()
})()


/**
 * @typedef {Object} Arguments
 * @prop {string} text The text to type.
 * @prop {string} app The app which to open.
 * @prop {string} [instant] Text to type instantly, before text.
 * @prop {number} [minDelay=10] The minimum delay with which to type.
 * @prop {number} [maxDelay=50] The maximum delay with which to type.
 * @prop {boolean} [help=false] Display help and quit.
 */
