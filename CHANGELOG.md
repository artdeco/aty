## 25 July 2018

### 1.1.2

- [doc] Remove final `hr`, key table section full section.

### 1.1.1

- [doc] Remove `---` between sections, remove border for example gif, optimise and put a comment on `doc/appshot-aty.gif`.

### 1.1.0

- [cli] Add a CLI
- [doc] Add more examples, better structure and format.
- [feature] Implement `e`, `w`; using either a string or variables.
- [package] Write more tags.

## 29 June 2018

### 1.0.0

- Create `aty` with [`mnp`][https://mnpjs.org]
- [repository]: `src`, `test`
