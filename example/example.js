/* yarn example */
import aty, { exec, type, typeInstant, activateApp } from '../src'

const s = `Hello World! \\
This is a new application that can type strings.`

;(async () => {
  const a = aty`
${activateApp`Code`}
${typeInstant`Welcome.`}
${type`${s}${10}${20}`}
  `
  await exec(a)
})()
