/* yarn example/code.js */
import { code } from '../src'

const a = code`36${'command'}`
const b = code`36${'command'}${'shift'}`

console.log(a)
console.log(b)
