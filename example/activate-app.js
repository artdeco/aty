/* yarn example/activate-app.js */
import { activateApp } from '../src'

const app = 'Terminal'

const a = activateApp`${app}`
const b = activateApp`Code`

console.log(a)
console.log(b)
