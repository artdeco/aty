/* yarn example/delay.js */
import { delay } from '../src'

const d = 100

const a = delay`${d}`
const b = delay`500`

console.log(a)
console.log(b)
