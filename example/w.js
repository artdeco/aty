/* yarn example/e.js */
import { e, w, activateApp, type } from '../src'

(async () => {
  await e([
    activateApp`Terminal`,
    type`echo hello world\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`date\n`,
  ])
})()