/* yarn example/keystroke.js */
import { keystroke } from '../src'

const a = keystroke`a${'command'}`
const b = keystroke`s${'alt'}${'command'}`

console.log(a)
console.log(b)
