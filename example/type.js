/* yarn example/type.js */
import aty, { type } from '../src'

const t = 'admin\\n'

const a = type`${t}`
const b = type`Though sympathy alone can't alter facts,
  it can help to make them more bearable.

${50}${100}${true}`

console.log(aty`
${a}
${b}
`)
