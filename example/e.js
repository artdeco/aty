/* yarn example/e.js */
import { e, activateApp, type } from '../src'

(async () => {
  await e([
    activateApp`Terminal`,
    type`echo hello world\n`,
  ])
})()