/* yarn example/hacker.js */
import { readFileSync } from 'fs'
import { resolve } from 'path'
import aty, {
  exec, type, typeInstant, activateApp, code, keystroke, delay,
} from 'aty'

const f = readFileSync(resolve(__dirname, __filename))

run()

async function run() {
  const a = aty`
  ${activateApp`Code`}
  ${typeInstant`/* recorded with appshot */`}
  ${type`${f}${15}${20}${true}`}
  ${keystroke`${'option'}${'shift'}f`}
  ${delay`2000`}
  ${keystroke`${'command'}a`}
  ${delay`1000`}
  ${code`51`}`

  await exec(a)
}
