/* yarn example/type-instant.js */
import aty, { typeInstant } from '../src'

const t = 'Type Instant Text'

const a = typeInstant`${t}`
const b = typeInstant`Do you believe in destiny?
That even the powers of time can be altered for a single purpose?`

console.log(aty`
${a}
${b}
`)
