/* yarn scripts/alamode.js */
import { e, w, activateApp,
  type } from '../src'

(async () => {
  await e([
    activateApp`Terminal`,
    type`mnp alamode -c\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`expensive alamode\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`expensive alamode.app -r\n`,
  ])
  await w()
  await e([
    activateApp`Terminal`,
    type`y\n`,
  ])
})()